package br.pucpr.ticketsAereos.tests.modificadores;

import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;

public class CiaAereaCreator extends Creator<CiaAerea, MassaTestesCiaAereaEnum> {

	private static CiaAereaCreator instance = new CiaAereaCreator();
	
	private CiaAereaCreator() {	}
	
	public CiaAerea createBlank(){
		return new CiaAerea();
	}
	
	@Override
	public CiaAerea create(MassaTestesCiaAereaEnum enumm) {
		// Cria o endereco
		CiaAerea ciaAerea = createBlank();

		// Atualiza as informacoes de acordo com o enum
		this.update(ciaAerea, enumm);

		// Retorna a CiaAerea
		return ciaAerea;
	}

	@Override
	public void update(CiaAerea ciaAerea, MassaTestesCiaAereaEnum enumm) {
		super.update(ciaAerea, enumm);
		
		ciaAerea.setNome(enumm.getNome());
	}
	
	@Override
	public void update(CiaAerea ciaAerea, String codigo) {
		ciaAerea.setNome(codigo + ciaAerea.getNome());
	}
	
	public static CiaAereaCreator getInstance() {
		return instance;
	}
}
