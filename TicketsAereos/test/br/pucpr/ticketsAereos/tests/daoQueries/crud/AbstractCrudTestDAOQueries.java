package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.PatternCrudBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.IdentifierInterface;
import br.pucpr.ticketsAereos.tests.verificador.Verificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public abstract class AbstractCrudTestDAOQueries<T extends IdentifierInterface, DTO extends Object, BC extends PatternCrudBC<T, DTO>, E extends Enum<E>> {

	///////////////////////////////////////////////////////////
	// Atributos
	///////////////////////////////////////////////////////////
	protected BC bc;
	protected DTO filter;
	protected Verificator<T, E> verificator;
	
	///////////////////////////////////////////////////////////
	// Metodos abstratos
	///////////////////////////////////////////////////////////
	protected abstract int getEnumSize();
	
	///////////////////////////////////////////////////////////
	// Teste do findAll e do findById
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		//Obtem todas as intancias de T
		Collection<T> objetos = bc.findAll();
		
		//Verifica se a quantidade eh a mesma dos testes de populate
		Assert.assertEquals(objetos.size(), getEnumSize());
		
		for (T objetoFindAll : objetos) {
			T objetoFindId = bc.findById(objetoFindAll.getId());
			verificator.verify(objetoFindAll, objetoFindId);
		}
		
		//buscando um id inexistente
		T objetoIdInvalido = bc.findById(1000000L);
		Assert.assertNull(objetoIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// DADOS BASICOS DO FILTRO
	///////////////////////////////////////////////////////////
		
	/**
	 * Realiza um teste com o filtro nulo, esperando que ocorram problemas
	 */
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		bc.findByFilter(null);
	}	
	
	/**
	 * Realiza um teste com o filtro vazio, esperando que ocorram problemas
	 */
	@Test(expected = BSIException.class)
	public void testFindByFilterVazio(){
		bc.findByFilter(filter);
	}	
}
