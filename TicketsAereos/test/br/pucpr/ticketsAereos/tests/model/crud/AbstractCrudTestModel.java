package br.pucpr.ticketsAereos.tests.model.crud;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.model.IdentifierInterface;
import br.pucpr.ticketsAereos.tests.modificadores.Creator;
import br.pucpr.ticketsAereos.tests.verificador.Verificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public abstract class AbstractCrudTestModel<T extends IdentifierInterface, E extends Enum<E>> {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected E objectEnum;
	protected E objectEnumTemp;
	protected T object;
	protected Creator<T, E> creator;
	protected Verificator<T, E> verificator;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	public AbstractCrudTestModel(E objectEnum) {
		System.out.println("\t\tValidando: " + objectEnum);
		this.objectEnum = objectEnum;
	}	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////

	@Before
	public void init() {
		this.object = creator.create(this.objectEnum);
	}

	// ////////////////////////////////////////
	// METODO DE TESTE JUNIT
	// ////////////////////////////////////////

	@Test
	public void criar() {
		// Verifica se os atributos estao preenchidos
		verificator.verify(object);

		// Alteracao dos atributos
		creator.update(object, objectEnumTemp);
		
		// Verifica se alteracao dos atributos esta sendo realizada
		verificator.verify(object, objectEnumTemp);
		
		//Volta os atributos, para ficar compativel com outros testes
		creator.update(object, objectEnum);
	}	
}
