package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;
import br.pucpr.ticketsAereos.tests.modificadores.CiaAereaCreator;
import br.pucpr.ticketsAereos.tests.verificador.CiaAereaVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

@RunWith(Parameterized.class)
public class TesteCiaAereaModel extends AbstractCrudTestModel<CiaAerea, MassaTestesCiaAereaEnum> {
	
	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesCiaAereaEnum.getParameters();
	}	
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteCiaAereaModel(MassaTestesCiaAereaEnum ciaAereaEnum) {
		super(ciaAereaEnum);
		creator = CiaAereaCreator.getInstance();
		verificator = CiaAereaVerificator.getInstance();
		objectEnumTemp = MassaTestesCiaAereaEnum.getInstance();
	}	

		
}
