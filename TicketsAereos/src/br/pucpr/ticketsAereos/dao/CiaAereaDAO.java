package br.pucpr.ticketsAereos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.ticketsAereos.dao.utils.Conexao;
import br.pucpr.ticketsAereos.dao.utils.NamedParameterStatement;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.CiaAerea;

public class CiaAereaDAO extends PatternCrudDAO<CiaAerea, CiaAereaDTO> {
	
	//////////////////////////////////////////
	// QUERIES
	//////////////////////////////////////////
	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO TB_CIA_AEREA ")
		.append("	(NOME) ")
		.append("VALUES ")
		.append("	(:nome)");
	
	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE TB_CIA_AEREA ")
		.append("SET NOME = :nome ")
		.append("WHERE ID = :id ");

	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM TB_CIA_AEREA ")
		.append("WHERE ID = :id ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
		.append("SELECT ID, NOME ")
		.append("FROM TB_CIA_AEREA ")
		.append("WHERE ID = :id ");
	
	private static StringBuilder selectAllSQL =  new StringBuilder()
		.append("SELECT ID, NOME ")
		.append("FROM TB_CIA_AEREA ");
	
	private static StringBuilder selectFilterSQL =  new StringBuilder()
		.append("SELECT C.ID, C.NOME ")
		.append("FROM TB_CIA_AEREA C ")
		.append("WHERE ");

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	
	private static CiaAereaDAO instance = new CiaAereaDAO();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	private CiaAereaDAO() {
	}
	
	//////////////////////////////////////////
	// METODOS
	//////////////////////////////////////////
	
	public static CiaAereaDAO getInstance() {
		return instance;
	}
	
	/////////////////////////////////////////
	// METODOS DML COM ALTERACAO NA BASE
	/////////////////////////////////////////
	
	@Override
	public void insert(CiaAerea object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			//Seta os parametros no map
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("nome", object.getNome());
			
			//Obtem o prepared statement
			ps = NamedParameterStatement.createPreparedStatementForInsert(connection, insertSQL.toString(), parameters);
			
			//Realiza a execucao do prepared statement
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
	}
	
	@Override
	public void update(CiaAerea object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			//Seta os parametros no map
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("nome", object.getNome());
			parameters.put("id", object.getId());
			
			//Obtem o prepared statement
			ps = NamedParameterStatement.createPreparedStatement(connection, updateSQL.toString(), parameters);
			
			//Realiza a execucao do prepared statement
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
	}

	@Override
	public void delete(CiaAerea object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			//Seta os parametros no map
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("id", object.getId());
			
			//Obtem o prepared statement
			ps = NamedParameterStatement.createPreparedStatement(connection, deleteSQL.toString(), parameters);
			
			//Realiza a execucao do prepared statement
			ps.executeUpdate();
			
			//Realiza o commit da execucao
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
	}
	
	/////////////////////////////////////////
	// METODOS DML DE RECUPERACAO DE INFORMACAO
	/////////////////////////////////////////	
	
	@Override
	public CiaAerea findById(Long id) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		CiaAerea ciaAerea = null;
		try {
			//Seta os parametros no map
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("id", id);
			
			//Obtem o prepared statement
			ps = NamedParameterStatement.createPreparedStatement(connection, selectIdSQL.toString(), parameters);
			
			//Realiza a execucao do prepared statement
			rs = ps.executeQuery();
			
			//Obtem as informacoes a partir do result set
			if(rs.next()){
				ciaAerea = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ciaAerea;
	}
	
	protected void populateFilter(CiaAereaDTO filtro, Map<String, Object> parameters, StringBuffer sb){
		if(StringUtils.isNotBlank(filtro.getNome())){
			sb.append("C.NOME = :nomeCiaAerea AND ");
			parameters.put("nomeCiaAerea", filtro.getNome());
		}
	}

	
	public Collection<CiaAerea> findByFilter(CiaAereaDTO filtro) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<CiaAerea> ciasAereas = new ArrayList<CiaAerea>();
		try {
			//Parte do filtro de parametros
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuffer sb = new StringBuffer(selectFilterSQL.toString());
			populateFilter(filtro, parameters, sb);
			
			//Obtem o prepared statement
			ps = NamedParameterStatement.createPreparedStatement(connection, sb.substring(0, sb.length() - 4), parameters);
			
			//Realiza a execucao do prepared statement
			rs = ps.executeQuery();

			//Obtem as informacoes a partir do result set
			while(rs.next()){
				ciasAereas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ciasAereas;
	}
	
	@Override
	public Collection<CiaAerea> findAll() {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<CiaAerea> ciaAerea = new ArrayList<CiaAerea>();
		try {
			//Obtem o prepared statement - nao possui argumentos logo pode ser utilizado o caminho normal
			ps = connection.prepareStatement(selectAllSQL.toString());
			
			//Realiza a execucao do prepared statement
			rs = ps.executeQuery();
			
			//Obtem as informacoes a partir do result set
			while(rs.next()){
				ciaAerea.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ciaAerea;
	}

	@Override
	protected CiaAerea populateObject(ResultSet rs) throws SQLException {
		CiaAerea endereco = new CiaAerea();
		endereco.setId(rs.getLong("ID"));
		endereco.setNome(rs.getString("NOME"));
		return endereco;
	}
}

