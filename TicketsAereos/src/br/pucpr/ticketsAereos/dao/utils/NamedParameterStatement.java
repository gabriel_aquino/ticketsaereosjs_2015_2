package br.pucpr.ticketsAereos.dao.utils;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Essa classe eh baseada em um artigo construido por Adam Crume para a revista Java World em Abril de 2007
 * (http://www.javaworld.com/article/2077706/core-java/named-parameters-for-preparedstatement.html)
 * 
 * Essa classe encapsula o conceito da classe {@link PreparedStatement} e permite ao desenvolvedor utilizar
 * parametros por nome ao inves do conceito de posicoes com o ?. Isso elimina qualquer confusao da representacao
 * de qual parametro utiliza determinada posicao.
 * 
 * Outro detalhe eh que pode ser trabalho com parametros repetidos, ou seja, adicionar em mais de uma posicao
 * um determinado parametro.
 * 
 * 
 * @author Everson Mauda
*/

public class NamedParameterStatement {
	
	////////////////////////////////////////////////////////////////
	// ATRIBUTOS
	////////////////////////////////////////////////////////////////
	
	//Cria um PreparedStatement interno para realizar a conexao
	private PreparedStatement statement;

	//Map para indicar as posicoes dos parametros
	private final Map<String, List<Integer>> mapPosicoesParametros = new HashMap<String, List<Integer>>();

	//Query SQL apos o parser de parametros de labels por posicoes
	private final String parsedQuery;
	
	////////////////////////////////////////////////////////////////
	// CONSTRUTORES
	////////////////////////////////////////////////////////////////
	
	/**
	 * Construtor para realizar o parser da query
	 * @param namedQuery - exemplo - SELECT * FROM TABELA WHERE ID = :ID
	 */
	private NamedParameterStatement(String namedQuery){
		 this.parsedQuery = parse(namedQuery);
	}
	
	/**
	 * Construtor que realiza
	 * @param connection the database connection
	 * @param query	  the parameterized query
	 * @throws SQLException if the statement could not be created
	 */
	private NamedParameterStatement(Connection connection, String query) throws SQLException {
		this(query);
		statement = connection.prepareStatement(parsedQuery);
	}

	/**
	 * Creates a NamedParameterStatement.  Wraps a call to
	 * c.{@link Connection#prepareStatement(java.lang.String) prepareStatement}.
	 * @param connection the database connection
	 * @param query	  the parameterized query
	 * @throws SQLException if the statement could not be created
	 */
	private NamedParameterStatement(Connection connection, String query, Integer RETURN_GENERATED_KEYS) throws SQLException {
		this(query);
		statement = connection.prepareStatement(parsedQuery, RETURN_GENERATED_KEYS);
	}

	////////////////////////////////////////////////////////////////
	// PARSER
	////////////////////////////////////////////////////////////////
	
	/**
	 * Realiza o parser de uma query com labels para uma query de parameter index
	 * Query Exemplo 1:
	 * namedQuery - SELECT * FROM TABELA WHERE ID = :id
	 * indexQuery - SELECT * FROM TABELA WHERE ID = ?
	 * 
	 * Query Exemplo 2:
	 * namedQuery - SELECT * FROM TABELA WHERE ID = :id AND NOME = :nome
	 * indexQuery - SELECT * FROM TABELA WHERE ID = ? AND NOME = ?
	 * 
	 * Query Exemplo 3:
	 * namedQuery - SELECT * FROM TABELA WHERE ID = :id AND NOME = :nome AND SOBRENOME = :sobrenome
	 * indexQuery - SELECT * FROM TABELA WHERE ID = ? AND NOME = ? AND SOBRENOME = ?

	 * @param namedQuery	query to parse
	 * @param mapPosicoesParametros map to hold parameter-index mappings
	 * @return the parsed query
	 */
	private String parse(String namedQuery) {
		int length = namedQuery.length();
		StringBuilder parsedQuery = new StringBuilder(length);
		boolean inSingleQuote = false;
		boolean inDoubleQuote = false;
		int position = 1;

		for(int i = 0; i < length; i++) {
			char c = namedQuery.charAt(i);
			
			//: Significa inicio de um rotulo de parametro
			//Nao esta em uma palavra com aspas simples ou duplas
			if(c == ':' && !inSingleQuote && !inDoubleQuote && i+1 < length &&
					Character.isJavaIdentifierStart(namedQuery.charAt(i+1))) {
				int j = i+2;
				
				while(j < length && Character.isJavaIdentifierPart(namedQuery.charAt(j))) {
					j++;
				}
				
				String name = namedQuery.substring(i+1, j);
				c='?'; // substitui o caracter c pelo parametro de index
				i += name.length(); // pula i ate o fim do nome do parametro

				List<Integer> indexList = mapPosicoesParametros.get(name);
				//Se o parametro ainda nao existir no set inicializa-o
				if(indexList == null) {
					indexList = new LinkedList<Integer>();
					mapPosicoesParametros.put(name, indexList);
				}
				indexList.add(position++);
			}
			
			//Adiciona o novo caractere a query passada pelo parser
			parsedQuery.append(c);
			
			if(c == '\'') {
				inSingleQuote = !inSingleQuote;
			} else if(c == '"') {
				inDoubleQuote = !inDoubleQuote;
			}
		}
		return parsedQuery.toString();
	}

	////////////////////////////////////////////////////////////////
	// SETAR PARAMETROS
	////////////////////////////////////////////////////////////////

	/**
	 * Returns the indexes for a parameter.
	 * @param name parameter name
	 * @return parameter indexes
	 * @throws IllegalArgumentException if the parameter does not exist
	 */
	private List<Integer> getIndexes(String name) {
		List<Integer> indexes = mapPosicoesParametros.get(name);
		if(indexes == null) {
			throw new IllegalArgumentException("Parameter not found: " + name);
		}
		return indexes;
	}

	/**
	 * Sets a parameter.
	 * @param name  parameter name
	 * @param value parameter value
	 * @throws SQLException if an error occurred
	 * @throws IllegalArgumentException if the parameter does not exist
	 * @see PreparedStatement#setObject(int, java.lang.Object)
	 */
	private void setObject(String name, Object value) throws SQLException {
		for(Integer position : getIndexes(name)){
			statement.setObject(position, value);
		}
	}

	/**
	 * Metodo que realiza o set de varios parametros que estao dentro de um MAP
	 * @param parameters
	 * @throws SQLException
	 */
	private void setParameters(Map<String, Object> parameters) throws SQLException {
		for(Entry<String, Object> entry : parameters.entrySet()){
			this.setObject(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Returns the underlying statement.
	 * @return the statement
	 */
	private PreparedStatement getStatement() {
		return statement;
	}
	
	////////////////////////////////////////////////////////////////
	// METODOS ESTATICOS
	////////////////////////////////////////////////////////////////
	
	/**
	 * Metodo que realiza a inicializacao do prepared statement juntamente com os parametros para
	 * um statement de insercao
	 * @param connection
	 * @param namedQuery
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public static PreparedStatement createPreparedStatementForInsert(Connection connection, 
				String namedQuery, Map<String, Object> parameters) throws SQLException{
		
		NamedParameterStatement nps = new NamedParameterStatement(connection, namedQuery, RETURN_GENERATED_KEYS);
		nps.setParameters(parameters);		
		return nps.getStatement();
		
	}
	
	/**
	 * Metodo que realiza a inicializacao do prepared statement juntamente com os parametros para
	 * um statement que n�o seja de insercao.
	 * 
	 * @param connection
	 * @param namedQuery
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public static PreparedStatement createPreparedStatement(Connection connection, 
			String namedQuery, Map<String, Object> parameters) throws SQLException{
		NamedParameterStatement nps = new NamedParameterStatement(connection, namedQuery);
		nps.setParameters(parameters);		
		return nps.getStatement();
	}
}