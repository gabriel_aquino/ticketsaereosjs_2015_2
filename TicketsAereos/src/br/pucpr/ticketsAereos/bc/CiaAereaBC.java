package br.pucpr.ticketsAereos.bc;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.ticketsAereos.dao.CiaAereaDAO;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.CiaAerea;

public class CiaAereaBC extends PatternCrudBC<CiaAerea, CiaAereaDTO> {
	
	///////////////////////////////////////////////////////////////////
	// Atributos
	///////////////////////////////////////////////////////////////////
	
	private static CiaAereaBC instance = new CiaAereaBC();
	
	///////////////////////////////////////////////////////////////////
	// Construtor - Singleton
	///////////////////////////////////////////////////////////////////
	
	private CiaAereaBC() {
		
	}

	public static CiaAereaBC getInstance() {
		return instance;
	}
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE MODIFICACAO
	///////////////////////////////////////////////////////////////////
	
	/**
	* Utilizado para realizar a validacao do object e a chamada do metodo de
	* armazenamento correspondente da classe DAO
	* 
	* Devera verificar se o objeto esta de acordo com as regras negociais
	* para ser atualizado na base de dados.
	* @param object
	*/
	public void insert(CiaAerea object){
		validateForDataModification(object);
		CiaAereaDAO.getInstance().insert(object);
	}
	
	/**
	* Utilizado para realizar a validacao do object e a chamada do metodo de
	* atualizacao correspondente na classe DAO.
	* 
	* Devera verificar se o objeto esta de acordo com as regras de negocio
	* para ser atualizado na base de dados.
	* @param object
	* @return
	*/
	public void update(CiaAerea object) {
		validateForDataModification(object);
		CiaAereaDAO.getInstance().update(object);
	}
	
	
	/**
	* Utilizado para chamar um metodo de delecao correspondente na classe DAO.
	* 
	* Devera verificar se o objeto passado nao eh null
	* @param object
	* @return
	*/
	public void delete(CiaAerea object) {
		if(object != null){
			CiaAereaDAO.getInstance().delete(object);
		}
	}
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE BUSCA
	///////////////////////////////////////////////////////////////////
	
	/**
	 * Utilizado para verificar se o id passado nao eh nulo e chamar o metodo
	 * de busca correspondente da classe DAO
	 * 
	 * Devera verificar se o id eh negativo ou null
	 * @param id
	 * @return
	 */
	public CiaAerea findById(Long id) {
		if(id < 0){
			return null;
		}
		return CiaAereaDAO.getInstance().findById(id);
	}
	
	/**
	 * Utilizado para buscas com o filtro da entidade, onde este contera as
	 * informacoes relacionadas com a filtragem de dados
	 * @param filter
	 * @return
	 */
	public Collection<CiaAerea> findByFilter(CiaAereaDTO filter) {
		if(!validateForFindData(filter)){
			throw new BSIException("E necessario preencher algum campo do filtro para realizar a pesquisa");
		}
		return CiaAereaDAO.getInstance().findByFilter(filter);
	}
	
	/**
	 * Utilizado para retornar todas as instancias de uma determinada classe,
	 * atraves do metodo de busca correspondente da classe DAO
	 * @return
	 */
	public Collection<CiaAerea> findAll() {
		return CiaAereaDAO.getInstance().findAll();
	}	
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE VALIDACAO
	///////////////////////////////////////////////////////////////////	
	
	@Override
	protected void validateForDataModification(CiaAerea object) {
		if(object == null){
			throw new BSIException("ER0040 - CiaAerea Nula");
		}
		
		if(StringUtils.isBlank(object.getNome())){
			throw new BSIException("ER0041 - Nome da CiaAerea nao preenchido");
		}
	}

	@Override
	protected boolean validateForFindData(CiaAereaDTO object) {
		return object != null && StringUtils.isNotBlank(object.getNome());
	}
}

