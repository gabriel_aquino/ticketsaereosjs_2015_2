package br.pucpr.ticketsAereos.view.model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Classe que representa uma abstracao da classe AbstractTableModel, com
 * algumas caracteristicas de evolucao de codigo.
 * 
 * @author mauda
 *
 * @param <E>
 */
public abstract class ViewAbstractTableModel<E> extends AbstractTableModel{
	
	private static final long serialVersionUID = 2636580625356765061L;
	protected List<E> linhas;
	
	// Necessario inicializar o array de colunas no construtor da classe filha
	protected String[] columns;
	
	/**
	 * Construtor basico que recebe a lista de objetos a serem populados na table
	 * @param linhas
	 */
	public ViewAbstractTableModel(List<E> linhas) {
		this.linhas = linhas;
	}
	
	public int getColumnCount() {
		return columns.length;
	}
	
	public int getRowCount() {
		return linhas.size();
	}
	
	@Override
	public String getColumnName(int column) {
		if(column < getColumnCount())
			return columns[column];
		return super.getColumnName(column);
	}	
	
	public E getValueAtRow(int row){
		return linhas.get(row);
	}
	
	public void setValueAtRow(int row, E object){
		linhas.set(row, object);
	}
}
